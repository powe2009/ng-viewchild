import { Component, ViewChild, OnInit, AfterContentInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { MessageComponent } from './message/message.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  inputMessage = 'abc';
  @ViewChild(MessageComponent, {static: false}) messageViewChild: MessageComponent;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.inputMessage = 'Hello World !';
  }

  ngAfterViewInit() {
    console.log(this.messageViewChild);
    this.messageViewChild.msg = 'Passed as View Child';
    this.cd.detectChanges();
  }
}
